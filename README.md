# HackerNews clone that has the following features:

- Display a list of links
- Search the list of links
- Users can authenticate
- Authenticated users can create new links
- Authenticated users can upvote links (one vote per link and user)
- Realtime updates when other users upvote a link or create a new one

## Frontend:

React: Frontend framework for building user interfaces

Apollo Client 2.1: Production-ready, caching GraphQL client
  
  - apollo-boost: offers some convenience by bundling several packages you need when working with Apollo Client:

    - apollo-client: Where all the magic happens
    - apollo-cache-inmemory: Our recommended cache
    - apollo-link-http: An Apollo Link for remote data fetching
    - apollo-link-error: An Apollo Link for error handling
    - apollo-link-state: An Apollo Link for local state management
    - graphql-tag: Exports the gql function for your queries & mutations
    - react-apollo contains the bindings to use Apollo Client with React.
    - graphql contains Facebook’s reference implementation of GraphQL - Apollo Client uses some of its functionality as well.

  - apollo-link-context: middleware for authenticating all Apollo requests
  - apollo-link-ws, subscriptions-transport-ws: for implementing subscriptions in with Apollo using web sockets 


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Table of Contents

- [Folder Structure](#folder-structure)
- [Available Scripts](#available-scripts)
  - [yarn start](#npm-start)  

## Folder Structure

After creation, your project should look like this:

```
my-app/
  README.md
  node_modules/
  package.json
  server/
    ...backend code
  src/
    components/
      SomeComponent/
        index.js
    constants.js
    index.js
    utils.js
```
## Available Scripts

In the project directory, you can run:

### `yarn start`

**_You must install the dependencies for and deploy the backend first_**

In your terminal, navigate to the server directory and execute the following commands:

```
yarn install
yarn prisma deploy
```

When prompted where (i.e. to which cluster) you want to deploy your service, choose any of the development clusters, e.g. public-us1 or public-eu1. (If you have Docker installed, you can also deploy locally.)

From the output of the deploy command, copy the HTTP endpoint and paste it into server/src/index.js, replacing the current placeholder __PRISMA_ENDPOINT__:

```
const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: '__PRISMA_ENDPOINT__',
      secret: 'mysecret123',
    }),
  }),
})
```

Navigate into the server directory and run the following commands to start the server:

```
yarn start
```

The yarn start executes the start script defined in package.json. The script first starts the server (which is then running on [http://localhost:4000](http://localhost:4000)).

Now navigate back into the original directory and run the following commands to start the server:

```
yarn start
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.